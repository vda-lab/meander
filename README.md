meander
=======

A visualization approach to explore the structural variome

Meander is a java standalone application for visualization of DNA read mapping data. It is mainly developed to visually discover and explore structural variations in a genome. Meander utilizes both a linear (1024 pixels) and a Hilbert curve-based representation (2D plane of 512x512 pixels) to visualize genomic variations. It is able to show data at 5 different resolution levels while it can additionally overlay already predicted variations from external software. It supports a comparison of four different samples against a common reference simultaneously while it is able to highlihght SVs  supported by double evidence (RD + PE).

Meander is a powerful, easy-to-use tool for comparative genomics aiming to aid researchers in identifying SVs that might be causative for various genetic disorders.
